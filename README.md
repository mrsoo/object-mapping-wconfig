# object-mapping-wconfig

## Giới thiệu

`object-mapping-wconfig` là một thư viện JavaScript nhẹ nhàng cung cấp một hàm tiện ích `mapObjectWithCfg` để ánh xạ một đối tượng đầu vào thành một đối tượng đầu ra dựa trên một bản đồ cấu hình.

## Cài đặt

Bạn có thể cài đặt thư viện thông qua npm:

npm install object-mapping-wconfig

## Sử dụng

```javascript
const { mapObjectWithCfg } = require("object-mapping-wconfig");

const inputObject = {
  a: 1,
  b: 2,
  c: 3,
};
const configMapping = {
  "testfield1.xxx": {
    mappingPath: "a",
    type: "number",
  },
  "testfield2.xxa": {
    mappingPath: "b",
    type: "number",
  },
  "testfield3.xxz": {
    mappingPath: "c",
    type: "number",
  },
};

const outputObject = mapObjectWithCfg(inputObject, configMapping);

console.log(outputObject);

//output:
// {
//   testfield1: { xxx: 1 },
//   testfield2: { xxa: 2 },
//   testfield3: { xxz: 3 }
// }
```
