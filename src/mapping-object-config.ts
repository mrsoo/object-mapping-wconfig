const qs = require("qs");

const { isEmpty, isEqual, merge, flattenDeep } = require("lodash");

type Primitive = string | number | boolean | null | never | never[];

interface KeyValueObject {
  [key: string]: Primitive;
}
const removeQuotes = (str: string): string =>
  str.replace(/^"(.+(?="$))"$/, "$1");
const isPrimitiveValue = (value: any) => {
  if (Array.isArray(value)) {
    return false;
  }
  if (["object", "function"].includes(typeof value) && !!value) {
    return false;
  }
  return true;
};

export const getObjectSchema = (
  obj: any,
  queryPath = "root"
): KeyValueObject => {
  const schema: any = {
    type: "object",
    queryPath,
    properties: {},
    required: [],
  };

  if (!obj) {
    schema.type = typeof obj;
    return schema;
  }

  Object.keys(obj).forEach((key) => {
    const value = obj[key];
    const valueType = typeof value;

    // Determine the type of the property value
    let propertyType;
    if (valueType === "number") {
      propertyType = "number";
    } else if (valueType === "string") {
      propertyType = "string";
    } else if (valueType === "boolean") {
      propertyType = "boolean";
    } else if (Array.isArray(value)) {
      propertyType = "array";
      if (value.length > 0) {
        const itemSchema = getObjectSchema(
          value?.[0],
          `${schema.queryPath}.${key}[]`
        );
        schema.properties[key] = {
          type: "array",
          queryPath: `${queryPath}`,
          items: itemSchema,
        };
      } else {
        schema.properties[key] = {
          type: "array",
          queryPath: `${queryPath}`,
          items: {},
        };
      }
    } else if (valueType === "object") {
      propertyType = "object";
      schema.properties[key] = getObjectSchema(
        value,
        `${schema.queryPath}.${key}`
      );
    } else {
      propertyType = "null";
    }

    // Add the property to the schema
    if (propertyType !== "null") {
      schema.properties[key] = schema.properties[key] || {};
      schema.properties[key].type = propertyType;
      schema.properties[key].queryPath = `${queryPath}.${key}`;
      if (propertyType !== "array" && value !== null) {
        schema.required.push(key);
      }
    }
  });

  if (schema.required.length === 0) {
    delete schema.required;
  }

  return schema;
};

export type IConfig = Record<
  string,
  {
    mappingPath: string;
    type: string;
  }
>;

export const castType = (value: any, typeCast: string, enc = true) => {
  if (enc) {
    return encodeURIComponent(JSON.stringify(value));
  }
  const urlPattern =
    /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/gi;
  if (urlPattern.test(value)) {
    return enc
      ? encodeURIComponent(JSON.stringify(value))
      : removeQuotes(decodeURIComponent(value));
  }
  let decodedValue;
  try {
    decodedValue = decodeURIComponent(value);
  } catch (e) {
    decodedValue = value;
  }
  switch (typeCast) {
    case "number":
      const tryParseValueRs = Number(removeQuotes(decodedValue));
      return Number.isNaN(tryParseValueRs)
        ? JSON.stringify(value)
        : tryParseValueRs;
    case "boolean":
      return Boolean(JSON.parse(decodedValue));
    case "string":
      return removeQuotes(String(decodedValue));
    default:
      return removeQuotes(decodedValue);
  }
};

const setValueByQuery = (
  obj: any,
  queryPath: string | string[],
  value: any
) => {
  const fields = Array.isArray(queryPath)
    ? queryPath
    : queryPath.split(/[.[\]]+/).filter(Boolean);
  // Duyệt qua từng trường cho đến trường cuối cùng
  fields.forEach((key: string, index: number) => {
    // Đi đến đối tượng con theo trường hiện tại
    if (index === fields.length - 1) {
      obj[key] = value;
    } else {
      obj = obj[key];
    }
  });
};

const castingDatatype = (castingObject: any) => {
  if (!castingObject) {
    return;
  }
  if (Array.isArray(castingObject)) {
    castingObject.forEach((item) => castingDatatype(item));
  }
  if (typeof castingObject === "object") {
    Object.keys(castingObject).forEach((key) => {
      if (
        Array.isArray(castingObject[key]) ||
        typeof castingObject[key] === "object"
      ) {
        if (isEqual(castingObject[key], [""])) {
          castingObject[key] = [];
          return;
        }
        castingDatatype(castingObject[key]);
      } else {
        if (castingObject[key] === null || castingObject[key] === undefined) {
          return;
        }
        if (!`${castingObject[key]}`.includes("@type:")) {
          return;
        }
        const [value, type] = `${castingObject[key]}`
          ?.split("@type:")
          .filter(Boolean);
        castingObject[key] = castType(value, type || typeof value, false);
      }
    });
  }
};

const invertConfigforRequest = (config: IConfig): IConfig =>
  Object.entries(config).reduce(
    (current, [targetPath, { mappingPath, type }]) => {
      return {
        ...current,
        [mappingPath]: {
          mappingPath: targetPath,
          type,
        },
      };
    },
    {}
  );

export const mapObjectWithCfg = (
  input: Record<string, any>,
  config: IConfig,
  isRequestSchema = false
): Record<string, any> => {
  let mappedObject: Record<string, any> = {};

  const inputIsArray = Array.isArray(input);
  const mappingRule = isRequestSchema ? invertConfigforRequest(config) : config;

  const resultObject = Object.entries(mappingRule).reduce(
    (current, [targetPath, { mappingPath, type }]) => {
      const sourceParts = mappingPath?.split(/[.[\]]+/)?.filter(Boolean);
      if (inputIsArray) {
        sourceParts?.unshift("input");
      }
      const targetParts = targetPath.split(/[.[\]]+/).filter(Boolean);

      // query path có []
      if (!targetPath.includes("[]")) {
        const sourceValue = sourceParts?.reduce(
          (value, part) => value?.[part],
          input
        );
        mappedObject = qs.parse(
          `${targetPath}=${castType(sourceValue, type || typeof sourceValue)}`,
          {
            allowDots: true,
          }
        );
        // #region Casting datatype
        setValueByQuery(
          mappedObject,
          targetParts,
          castType(sourceValue, type || typeof sourceValue, false)
        );
        // #endregion
      } else {
        // handle with array
        const sourceValues = sourceParts?.reduce(
          (prevSourceValue: any, part, index) => {
            // primitiveVal: use for array of primitive
            if (index === sourceParts.length - 1) {
              /// the ref prop always primitive type
              if (prevSourceValue.isArr) {
                // map -> {path, [field]: value}
                const convertOriginalObject = Object.keys(prevSourceValue)
                  .filter((x) => Number.isInteger(Number(x)))
                  .map((x) => ({
                    path: `${prevSourceValue[x].path}${
                      Number.isInteger(+part) ? "" : `.${part}`
                    }`,
                    value: Number.isInteger(+part)
                      ? prevSourceValue[x].primitiveVal
                      : prevSourceValue[x][part],
                    type,
                  }));

                return convertOriginalObject;
              }
              return {
                path: prevSourceValue.path,
                value: prevSourceValue[part],
                type,
              };
            }
            const mappingPart = (mappingPath?.split(/[.]+/)?.filter(Boolean) ||
              [])?.[index];

            const currentPathValue = prevSourceValue.isArr
              ? flattenDeep(
                  Object.keys(prevSourceValue)
                    .filter((x) => Number.isInteger(Number(x)))
                    .map((key) => prevSourceValue[key])
                    .map((item) => {
                      const valByPart = Array.isArray(item[part])
                        ? //set path if item.[path] is array
                          item[part].map((subItem: any, iSub: number) => ({
                            path: `${item.path}.${part}[${iSub}]`,
                            ...(isPrimitiveValue(subItem)
                              ? { primitiveVal: subItem }
                              : subItem),
                          }))
                        : // item.[path] undefined possible here => use part from mappingPath
                          {
                            path: `${item.path}.${mappingPart}`,
                            ...item[part],
                          };
                      return valByPart;
                    })
                )
              : prevSourceValue[part];

            if (Array.isArray(currentPathValue)) {
              const reduceCurrentPathValue = currentPathValue.reduce(
                (prevData, cur, i) => ({
                  ...prevData,

                  [i]: {
                    path: `${
                      prevSourceValue?.path ? `${prevSourceValue?.path}.` : ""
                    }${part}[${i}]`,
                    ...cur,
                  },
                }),
                {}
              );
              const indexedObjectArray = {
                path: `${
                  prevSourceValue?.path ? `${prevSourceValue?.path}.` : ""
                }${part}`,
                isArr: true,
                ...reduceCurrentPathValue,
              };
              return indexedObjectArray;
            }

            return {
              path: part,
              ...currentPathValue,
            };
          },
          inputIsArray ? { input } : input
        );
        let queryString = "";

        const buildQueryData = (
          Array.isArray(sourceValues) ? sourceValues : []
        )?.map((sourceValue: any) => {
          const {
            path: sourceValuePath,
            type: sourceValueType,
            value,
          } = sourceValue || {};
          const sParts = sourceValuePath.split(/[.]+/).filter(Boolean) || [];
          const tgParts = targetPath.split(/[.]+/).filter(Boolean) || [];

          // find index of array because amount of arr is same
          const arrPartsOnTargetIndex = tgParts
            .map((x, i) => (/[.[\]]+/.test(x) ? i : null))
            .filter((x) => Number.isInteger(x));
          const arrPartsOnSourceIndex = sParts
            .map((x: string, i: any) => (/[.[\]]+/.test(x) ? i : null))
            .filter((x: any) => Number.isInteger(x));

          const obj = arrPartsOnTargetIndex.reduce(
            (old: any, val, i) => [
              ...old,
              { target: val, source: arrPartsOnSourceIndex[i] },
            ],
            []
          );
          // base on source then transform
          obj.forEach(({ target, source }: any) => {
            tgParts[target!] = sParts[source!].replace(
              sourceParts[source!],
              targetParts[target!]
            );
          });
          return {
            path: tgParts.join("."),
            value,
            type: sourceValueType,
          };
        });

        buildQueryData.forEach(
          (
            element: { path: string; value: any; type: string },
            index: number
          ) => {
            if (!element.path.includes("[]")) {
              queryString += `${index !== 0 ? "&" : ""}${
                element.path
              }=${castType(
                element.value,
                element.type || typeof element.value
              )}@type:${type}`;
            } else {
              const path = element?.path?.substring(
                0,
                element?.path?.lastIndexOf("[]") + 2
              );
              queryString += `${index !== 0 ? "&" : ""}${path}=`;
            }
          }
        );

        queryString =
          isEmpty(queryString) && isEmpty(sourceValues)
            ? targetPath.substring(0, targetPath?.lastIndexOf("[]") + 2)
            : queryString;

        mappedObject = qs.parse(queryString, {
          allowDots: true,
          arrayLimit: Number.MAX_VALUE,
          depth: Number.MAX_VALUE,
        });
        // #region Casting datatype
        castingDatatype(mappedObject);
        // #endregion
      }
      return merge(current, mappedObject);
    },
    mappedObject
  );
  return resultObject;
};
