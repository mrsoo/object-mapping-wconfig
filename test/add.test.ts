import { mapObjectWithCfg } from "../src";

test("mapObjectWithCfg function", () => {
  const inputObject = {
    a: 1,
    b: 2,
    c: 3,
  };
  const configMapping = {
    testfield1: {
      mappingPath: "a",
      type: "number",
    },
    testfield2: {
      mappingPath: "b",
      type: "number",
    },
    testfield3: {
      mappingPath: "c",
      type: "number",
    },
  };

  const outputObjectResult = { testfield1: 1, testfield2: 2, testfield3: 3 };

  const outputObject = mapObjectWithCfg(inputObject, configMapping);

  expect(outputObject).toEqual(outputObjectResult);
});
